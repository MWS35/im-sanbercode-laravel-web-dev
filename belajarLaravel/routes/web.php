<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/master', [TemplateController::class, 'master']);

Route::get('/table', [TemplateController::class, 'table']);

Route::get('/data-tables', [TemplateController::class, 'data_tables']);

// CRUD CAST
// CREATE CAST
Route::get('/cast/create', [CastController::class, 'create']);
// KIRIM DATA KE DATABASE
Route::post('/cast', [CastController::class, 'store']);

// READ CAST
Route::get('/cast', [CastController::class, 'index']);
// DETAIL DATA BIO
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// UPDATE CAST
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// KIRIM DATA UPDATE
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
// DELETE CAST
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);