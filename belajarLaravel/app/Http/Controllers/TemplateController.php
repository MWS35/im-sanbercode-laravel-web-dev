<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function master()
    {
        return view('contoh.master');
    }

    public function table()
    {
        return view('halaman.table');
    }

    public function data_tables()
    {
        return view('halaman.data-tables');
    }
}
