@extends('contoh.master')

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a><br>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">Nomor</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur (Tahun)</th>
            <th scope="col">Bio</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
                <tr>
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                </tr> 
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
      </table>
@endsection