@extends('contoh.master')

@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-info btn-sm">Kembali</a>
@endsection