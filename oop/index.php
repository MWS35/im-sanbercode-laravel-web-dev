<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $shaun = new animal("Shaun");
    echo "name : $shaun->name <br>";
    echo "legs : $shaun->legs <br>";
    echo "cold blooded : $shaun->cold_blooded <br>";
    
    echo "<br>";
    
    $sunwokong = new ape("Kera Sakti");
    echo "name : $sunwokong->name <br>";
    echo "legs : $sunwokong->legs <br>";
    echo "cold blooded : $sunwokong->cold_blooded <br>";
    echo "yell : $sunwokong->yell <br>";
    
    echo "<br>";
    
    $kodok = new frog("Kuduk");
    echo "name : $kodok->name <br>";
    echo "legs : $kodok->legs <br>";
    echo "cold blooded : $kodok->cold_blooded <br>";
    echo "jump : $kodok->jump <br>";
?>